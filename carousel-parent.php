<?php
/*
 * Template Name: Parent page template
 */


get_header(); ?>
	
	<div id="main">
		
		<div class="info-block alignleft">
		
			<div class="bread-crumbs">
				<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
			</div>
			
			
			<div class="left-block">
				<div style="position: relative; height: 338px;">
					<!-- Define elements to accept the alt and title text from the images. -->
	
					<p id="alt-text"></p>
					<p id="title-text"></p>
				
					<!-- Define left and right buttons. -->
					<input id="left-but"  type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-left.png" />
					<input id="right-but" type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png" />
				
					<div class="carousel-bar">
						
						<?php 
							$args = array(
								'post_parent' => $post->ID,
								'post_type' => 'page',
								'post_status' => 'publish',
								'posts_per_page' => -1,
								'meta_key' => 'order_on_parent',
								'orderby' => 'meta_value_num',
								'order' => 'ASC'
							);
							$children = new WP_Query($args);
							if ( $children->have_posts() ) {
								while ( $children->have_posts() ) { 
									$children->the_post(); 
								
								/*remove_filter('excerpt_length', 'my_excerpt');
								add_filter('excerpt_length', 'tax_excerpt_length');
								remove_filter('wp_trim_excerpt', 'trim_excerpt');
								add_filter('wp_trim_excerpt', 'new_excerpt');*/
								$titles = get_post_meta(get_the_ID(), 'Title for Parent');
								if (!empty($titles)) {
									foreach ($titles as $title) {
										$titles = explode("\n", $title);
									}
								}
								
								$descriptions = get_post_meta(get_the_ID(), 'Description for Parent');

								if (!empty($descriptions)) {
									foreach ($descriptions as $description) {
										$descriptions = explode("\n", $description);
									}
								}
								
								if ($titles[0]) {$page_title = $titles[0];} else {$page_title = '';}
								if ($descriptions[0]) {$page_desc = $descriptions[0];} else {$page_desc = '';}
								
								?>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('full', array(
										'class' => 'cloudcarousel', 
										'alt' => $page_title,//get_the_title(), 
										'title' => $page_desc//get_the_excerpt()
									)); ?>
								</a>
								<?php 
								/*remove_filter('wp_trim_excerpt', 'new_excerpt');
								add_filter('wp_trim_excerpt', 'trim_excerpt');
								remove_filter('excerpt_length', 'tax_excerpt_length');
								add_filter('excerpt_length', 'my_excerpt'); */
									
								}
							}
							wp_reset_query();
						?>
						
						
					</div>
				</div>
				<div class="single-main alignleft ">
					<?php 
						if (have_posts()) {
							while (have_posts()) {
								the_post();?>
								<div class="post">
									<h2 class="title"><?php the_title();?> </h2>
									<div class="content"> <?php the_content(); ?> </div>
								</div>
							<?php }
						}
					?>
				</div>
			</div>
			
		</div>
		
		<?php get_sidebar('right'); ?>
	</div>
<?php get_footer(); ?>