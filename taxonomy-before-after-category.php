<?php get_header(); ?>
	<div id="main">
			<div class="main alignleft">
				
				<div class="bread-crumbs">
					<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
				</div>
				
				
				
				<?php 
					$term =	$wp_query->queried_object;
					$description = $term->description;
					$title = $term->name;
					$count_posts = $term->count;
				?>
				



				<?php  if (is_parent_category()) { ?>
					<?php  if ($count_posts != 0 ) { ?>
					
					<div style="position: relative; height: 338px;" <?php if ($count_posts == 1) { echo 'class="one-modification"'; } ?>>
					
					<!-- Define elements to accept the alt and title text from the images. -->
	
					<p id="alt-text"></p>
					<p id="title-text"></p>
				
					<!-- Define left and right buttons. -->
					<input id="left-but"  type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-left.png" />
					<input id="right-but" type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png" />
					<div class="<?php if ( $count_posts == 1 ) { echo 'term-carousel-one'; } else { echo 'term-carousel-bar';} ?>">
					
						<?php
							$term_children = get_term_children( $term->term_id, 'before-after-category' ); 
							foreach ($term_children as $one_term) {
								$args = array(
									'order'=> 'ASC',
									'post_type'=>'modification',
									'posts_per_page' => 1,
									'post_status' => 'publish',
									'tax_query' => array(
										array (
											'taxonomy' => 'before-after-category',
											'field' => 'id',
											'terms' => $one_term
										)
									)
								);
								
								$loop = new WP_Query($args);
								
								if( $loop->have_posts() ) {
									while ( $loop->have_posts() ) { 
										$loop->the_post();
										$cur_term = get_term_by('id', $one_term, 'before-after-category'); ?>
										
										
										
										<?php if (get_post_meta(get_the_ID(), "modification_after-image_thumbnail_id", $single = true) && get_post_meta(get_the_ID(), "modification_before-image_thumbnail_id", $single = true)) {
									
										$term = wp_get_post_terms(get_the_ID(), 'before-after-category', array("fields" => "all"));
										$term_id = $term[0]->term_taxonomy_id;
										$term_array = get_term( $term_id, 'before-after-category' );
										$term_name = $term_array->name;
										
										
										remove_filter('excerpt_length', 'my_excerpt');
										add_filter('excerpt_length', 'tax_excerpt_length');
										remove_filter('wp_trim_excerpt', 'trim_excerpt');
										add_filter('wp_trim_excerpt', 'new_excerpt');
											$excerpt = get_the_excerpt();
										remove_filter('wp_trim_excerpt', 'new_excerpt');
										add_filter('wp_trim_excerpt', 'trim_excerpt');
										remove_filter('excerpt_length', 'tax_excerpt_length');
										add_filter('excerpt_length', 'my_excerpt');
									
									
										$features = get_post_meta(get_the_ID(), 'merged_image');
										if (!empty($features)) {?>
											
												<?php foreach ($features as $feature) {
													$feature = explode("\n", $feature);?>
													<a href="<?php echo site_url() . '/before-after-category/' . $cur_term->slug; ?>">
														<img class="cloudcarousel" title="<?php echo $excerpt; ?>" src=" <?php echo $feature[0] ?>" alt="Before and After <?php if ($term_name != '') {echo ' | ' . $term_name;} ?> <?php echo ' | ' . get_the_title(); ?>" />
													</a>
												<?php } 
											}
										}
									}
								}
								wp_reset_query();
							}
							
						?>
					<?php } ?>
						</div>
					</div>
				<?php } else {  ?>
			
				<?php if ($count_posts != 0) { ?>
					<div style="position: relative; height: 338px;" <?php if ($count_posts == 1) { echo 'class="one-modification"'; } ?>>
					
					<!-- Define elements to accept the alt and title text from the images. -->
	
					<p id="alt-text"></p>
					<p id="title-text"></p>
				
					<!-- Define left and right buttons. -->
					<input id="left-but"  type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-left.png" />
					<input id="right-but" type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png" />
					
						<div class="<?php if ( $count_posts == 1 ) { echo 'term-carousel-one'; } else { echo 'term-carousel-bar';} ?>">
							
							<?php 
								$args = array(
									'order'=> 'ASC',
									'post_type'=>'modification',
									'posts_per_page' => -1,
									'post_status' => 'publish',
									'tax_query' => array(
										array (
											'taxonomy' => 'before-after-category',
											'field' => 'id',
											'terms' => $term->term_id
										)
									)
								);
								$the_query = new WP_Query( $args );
								
								if( $the_query->have_posts() ) {
									while ( $the_query->have_posts() ) { 
										$the_query->the_post();?>
																		
										<?php if (get_post_meta(get_the_ID(), "modification_after-image_thumbnail_id", $single = true) && get_post_meta(get_the_ID(), "modification_before-image_thumbnail_id", $single = true)) {
									
										$term = wp_get_post_terms(get_the_ID(), 'before-after-category', array("fields" => "all"));
										$term_id = $term[0]->term_taxonomy_id;
										$term_array = get_term( $term_id, 'before-after-category' );
										$term_name = $term_array->name;
										
										
										remove_filter('excerpt_length', 'my_excerpt');
										add_filter('excerpt_length', 'tax_excerpt_length');
										remove_filter('wp_trim_excerpt', 'trim_excerpt');
										add_filter('wp_trim_excerpt', 'new_excerpt');
											$excerpt = get_the_excerpt();
										remove_filter('wp_trim_excerpt', 'new_excerpt');
										add_filter('wp_trim_excerpt', 'trim_excerpt');
										remove_filter('excerpt_length', 'tax_excerpt_length');
										add_filter('excerpt_length', 'my_excerpt');
									
									
										$features = get_post_meta(get_the_ID(), 'merged_image');
										if (!empty($features)) {?>
											
												<?php foreach ($features as $feature) {
													$feature = explode("\n", $feature);?>
													<img class="cloudcarousel" title="<?php echo $excerpt; ?>" src=" <?php echo $feature[0] ?>" alt="Before and After <?php if ($term_name != '') {echo ' | ' . $term_name;} ?> <?php echo ' | ' . get_the_title(); ?>" />
												<?php } ?>
											
										<?php } 
										}
									}
								}
							?>
	
						</div>
					</div>
					<?php } ?>
					
					<?php  } ?>
					
					<h2 class="title"><?php echo $title; ?></h2>
					<div class="post clearfloat">
						<?php echo $description; ?>
					</div>
					

				<?php get_sidebar('blog'); ?>
			</div>
		<?php get_sidebar('right'); ?>
		
		
	</div>
	
<?php get_footer(); ?>