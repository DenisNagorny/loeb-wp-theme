<div id="image-sidebar">
		<?php 
			$args = array(
				'menu' => 'Home Menu',
				'container' => '',
				'menu_class'=> 'home-menu',
				//'walker' => $walker
			);
			 wp_nav_menu($args);

		?>
	<?php
		if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'home-images' ) ) {
			dynamic_sidebar('home-images') ;
		}
	?>
</div>