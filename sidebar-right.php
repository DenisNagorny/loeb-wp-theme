<div id="right-sidebar">
	<?php get_search_form(); ?>
	<?php
		if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'right' ) ) {
			dynamic_sidebar('right') ;
		}
	?>
</div>