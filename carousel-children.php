<?php
/*
 * Template Name: Children Page
 */


get_header(); ?>
	
	<div id="main">
		
		<div class="info-block alignleft">
		
			<div class="bread-crumbs">
				<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
			</div>
			
			
			<div class="left-block">
				<div style="position: relative; height: 338px;">
					<!-- Define elements to accept the alt and title text from the images. -->
	
					<p id="alt-text"></p>
					<p id="title-text"></p>
				
					<!-- Define left and right buttons. -->
					<input id="left-but"  type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-left.png" />
					<input id="right-but" type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png" />
				
					<div class="<?php $id = $post->ID; if ( $id == 1174 ) { echo 'id1174 '; } else { echo ' carousel-bar';} ?>">
						<?php 
							$features = get_post_meta(get_the_ID(), 'carousel_image');

							if (!empty($features)) {
								foreach ($features as $feature) {
									$fields = explode("\n", $feature); 
									if ($fields[0]) { $i = $fields[0]; } else { $i = ''; }
									if ($fields[1]) { $src = $fields[1]; } else { $src = ''; }
									if ($fields[2]) { $alt = $fields[2]; } else { $alt = ''; }
									if ($fields[3]) { $title = $fields[3]; } else { $title = ''; }
									
									$images[] = array(
										"id" => $i, 
										"src" => $src, 
										"alt" => $alt, 
										"title" => $title
									);
								 }
							}
							/*print_r($images);
							function myCmp($a, $b) { 
								if ($a['id'] === $b['id']) return 0; 
								return $a['id'] > $b['id'] ? 1 : -1; 
							} 
							
							uasort($images, 'myCmp');*/
							
							function build_sorter($key) {
								return function ($a, $b) use ($key) {
									return strnatcmp($a[$key], $b[$key]);
								};
							}
							
							usort($images, build_sorter('id'));
							
							
			
							
							//print_r($images);
							
							foreach ($images as $image) { ?>
								<img class="cloudcarousel"
									src = "<?php echo $image['src']; ?>"
									alt = "<?php echo $image['alt']; ?>"
									title = "<?php echo $image['title']; ?>"
								/>
							<?php 
							}
							
							
							
						?>
					</div>
				</div>
				<div class="single-main alignleft ">
					<?php 
						if (have_posts()) {
							while (have_posts()) {
								the_post();?>
								<div class="post">
									<h2 class="title"><?php the_title();?> </h2>
									<div class="content"> <?php the_content(); ?> </div>
								</div>
							<?php }
						}
					?>
				</div>
			</div>
		
		</div>
		
		<?php get_sidebar('right'); ?>
	</div>
<?php get_footer(); ?>