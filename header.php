<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <!--[if IE]><![endif]-->
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
		<title>
			<?php wp_title('&laquo;', true, 'right'); ?>
			<?php bloginfo('name'); ?>
		</title>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="shortcut icon" href="/favicon.ico" />
		<?php
			if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
				wp_head();
				global $user_app;
				if (isset($user_app)) echo $user_app->ExecuteController('header');
			?>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css" />

	</head>
<!--[if lt IE 7 ]> <body <?php body_class('ie6'); ?>> <![endif]-->
<!--[if IE 7 ]>  <body <?php body_class('ie7'); ?>>   <![endif]-->
<!--[if IE 8 ]>  <body <?php body_class('ie8'); ?>>   <![endif]-->
<!--[if IE 9 ]>  <body <?php body_class('ie9'); ?>>   <![endif]-->
<!--[if !IE]><!--> <body <?php body_class(); ?>> <!--<![endif]-->

<div class="seo-wrap">
	<h1><?php global $aiosp; echo $aiosp->orig_title; ?></h1>
</div>
<div class="header">
	<div class="text-block">
		<a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-header.png" alt="" /></a>
	</div>
	
	<?php
	
		$args = array(
			'menu' => 'Top Menu',
			'echo' => 0,
			'container' => '',
			'menu_class'=> 'top-menu',
		);
		echo wp_nav_menu($args);

	?>
</div>

<div class="wrap">