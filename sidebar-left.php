<?php if ((is_page() || is_front_page())) { ?>
<div class="left-sidebar">
	
	<?php
		if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'left' ) ) {
			dynamic_sidebar('left') ;
		}
	?>
	<div class="widget widget-subpages">
		<?php if (!is_front_page() ) { ?><div class="title"><?php echo get_the_title($post->post_parent); ?></div><?php } ?>
		<?php if (! is_front_page() ) {  ?>	
			
			<?php
				if (is_page_template('contact-page.php')) {
					$args = array(
						'depth'        => 2,
						'child_of'     => $post->ID,
						'post_type'    => 'page',
						'post_status'  => 'publish',
						'title_li'     => __(''),
						'sort_order'   => 'DESC',
						//'sort_column'  => 'menu_order',
						'exclude'      => $post->post_parent,
						'orderby'      => 'title'
					);
					echo '<ul>';
					wp_list_pages($args);
					echo '</ul>'; 
				}
			?>
			
			<?php
				if ((is_page() && has_children($post->ID)) && !is_front_page() && (!is_page_template('contact-page.php'))) {
					$args = array(
						'depth'        => 1,
						'child_of'     => $post->ID,
						'post_type'    => 'page',
						'post_status'  => 'publish',
						'title_li'     => __(''),
						//'sort_order'   => 'DESC',
						//'sort_column'  => 'menu_order',
						'exclude'      => $post->post_parent,
						'orderby'      => 'title'
					);
					echo '<ul>';
					wp_list_pages($args);
					echo '</ul>';
				}

				if ((is_page() && !has_children($post->ID)) && !is_front_page() && (!is_page_template('contact-page.php'))) {
					$args = array(
						'depth'        => 1,
						'child_of'     => $post->post_parent,
						'post_type'    => 'page',
						'post_status'  => 'publish',
						'title_li'     => __(''),
						'sort_order'   => 'DESC',
						//'sort_column'  => 'menu_order',
						'exclude'      => $post->post_parent,
						'orderby'      => 'title'
					);
					echo '<ul>';
					wp_list_pages($args);
					echo '</ul>';
				}
				
		} ?>
				
		
		
		<?php /* if (is_front_page()) { ?>
			<div class="title"><a href="<?php echo get_permalink(61); ?>"><?php echo get_the_title(61); ?></a></div>
			<?php 		
				
					$args = array(
						'depth'        => 1,
						'child_of'     => 61,
						'post_type'    => 'page',
						'post_status'  => 'publish',
						'title_li'     => __(''),
						'sort_order'   => 'DESC',
						'sort_column'  => 'menu_order',
						'exclude'      => $post->post_parent
					);
					echo '<ul>';
					wp_list_pages($args);
					echo '</ul>';
				?>
				<div class="title"><a href="<?php echo get_permalink(63); ?>"><?php echo get_the_title(63); ?></a></div>
				<?php 
					$args = array(
						'depth'        => 1,
						'child_of'     => 63,
						'post_type'    => 'page',
						'post_status'  => 'publish',
						'title_li'     => __(''),
						'sort_order'   => 'DESC',
						'sort_column'  => 'menu_order',
						'exclude'      => $post->post_parent
					);
					echo '<ul>';
					wp_list_pages($args);
					echo '</ul>';
				?>
				
		<?php } */ ?>
		
	</div>
	
	<?php if (is_front_page()) { ?>
	<div class="left-menu">
		<?php 
			$args = array(
					'menu' => 'Home Left Menu',
					'container' => '',
					'menu_class'=> 'home-left-menu',
				);
			wp_nav_menu($args);
		?>
	</div>
	<?php } ?>
	
	
</div>
<?php } ?>