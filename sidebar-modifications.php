<div class="sidebar-modifications">
	<?php
		if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'modifications' ) ) {
			dynamic_sidebar('modifications');
		}
	?>
</div>
