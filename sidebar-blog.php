<div class="sidebar-blog">
	<?php
		if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'blog' ) ) {
			dynamic_sidebar('blog') ;
		}
	?>
</div>