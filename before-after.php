<?php 
/*
 * Template Name: Before-After Page
 */
	get_header();
?>
	
	<div id="main">
		<?php get_sidebar('carousel'); ?>
		<div class="main clearfloat">
			<div class="content-block">
				<div class="main-title"><?php the_title(); ?></div>
				<?php get_sidebar('modifications'); ?>
			</div>
			<?php get_sidebar('right'); ?>
		</div>
		
	</div>
	
<?php get_footer(); ?>