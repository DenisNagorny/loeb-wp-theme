<?php get_header(); ?>
	<div id="main">
			<div class="main alignleft">
				<h2 class="title">Aesthetic News</h2>
				<?php 
					if (have_posts()) {
							while (have_posts()) {
								the_post();
								?>
								<div class="post clearfloat <?php if (has_post_thumbnail())  { echo 'has_thumb'; } ?>">
									
									<h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
									<div class="meta">
										<?php the_author(); ?> | <?php the_time("F j, Y") ?> | Posted In <?php the_category(', '); ?>
									</div>
									
									<div class="clearfloat">
										<?php if (has_post_thumbnail()) { ?>
											<div class="featured-image alignleft">
												<?php the_post_thumbnail( 'post-thumbnail'); ?>
											</div>
										<?php }?>
										<div class="content alignleft"><?php the_excerpt(); ?></div>
									</div>
									
								</div> 
						<?php   }
					}
				?>
				<div class="nav clearfloat">
					<div class="prev alignleft"><?php previous_posts_link('&lt;&lt; Older Entries') ?></div>
					<div class="next alignright"><?php next_posts_link('New Entries &gt;&gt;') ?></div>
				</div>
				<?php get_sidebar('blog'); ?>
			</div>
		<?php get_sidebar('right'); ?>
		
		
	</div>
	
<?php get_footer(); ?>