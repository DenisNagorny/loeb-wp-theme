<?php get_header(); ?>
	
	<div id="main">
			<div class="main alignleft">
			
				<div class="bread-crumbs">
						<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
				</div>
				
				<?php 
					if (have_posts()) {
							while (have_posts()) {
								the_post();
								?>
								<div class="post clearfloat <?php if (has_post_thumbnail())  { echo 'has_thumb'; } ?>">
									
									<h2 class="title"><?php the_title();?></h2>
									<div class="meta">
										<?php the_author(); ?> | <?php the_time("m/d/y") ?> | Posted In <?php the_category(', '); ?>
									</div>
									<div class="content">
										<?php if (has_post_thumbnail()) { ?>
											<div class="featured-image alignleft"><?php the_post_thumbnail( 'single-image'); ?></div>
										<?php }?>
										<?php the_content(); ?>
										</div>
								</div> 
						<?php   }
					}
				?>
				
			</div>
		<?php get_sidebar('right'); ?>
	</div>
	
<?php get_footer(); ?>