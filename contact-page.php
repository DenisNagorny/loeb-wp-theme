<?php 
/*
 * Template Name: Contact Page
 */
	get_header();
?>
	
	<div id="main">
		<div class="left-block">
				<div class="bread-crumbs">
						<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
				</div>
			
			<?php get_sidebar('left'); ?>
			<div class="single-main alignleft">
				<?php 
					if (have_posts()) {
							while (have_posts()) {
								the_post();
								?>
					<div class="post">
						<h2 class="title"><?php the_title(); ?></h2>
						<div class="content"><?php the_content(); ?></div>
					</div>
				<?php	}
					}
				?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>