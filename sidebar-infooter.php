<div id="sidebar-in-footer">
	<?php
		if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'infooter' ) ) {
			dynamic_sidebar('infooter') ;
		}
	?>
</div>