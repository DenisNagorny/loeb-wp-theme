<div style="position: relative; height: 338px;">
	<!-- Define elements to accept the alt and title text from the images. -->
	
	<p id="alt-text"></p>
	<p id="title-text"></p>

	<!-- Define left and right buttons. -->
	<input id="left-but"  type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-left.png" />
	<input id="right-but" type="image" src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png" />

	<div class="main-carousel-bar">
		
		<?php 
			/*$args = array(
				'order'=> 'ASC',
				'post_type'=>'modification',
				'posts_per_page' => -1,
				'tax_query' => array(
					array (
						'taxonomy' => 'before-after-category',
						'field' => 'slug',
						'terms' => 'feature'
					)
				)
			);
			$the_query = new WP_Query( $args );
			if( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) { 
					$the_query->the_post();
					
					if (get_post_meta(get_the_ID(), "modification_after-image_thumbnail_id", $single = true) && get_post_meta(get_the_ID(), "modification_before-image_thumbnail_id", $single = true)) {
						
						$term = wp_get_post_terms(get_the_ID(), 'before-after-category', array("fields" => "all"));
						
						//print_r($term);
						
						$term_id = $term[0]->term_taxonomy_id;
						
						if ( $term_id == 50 ) {
							$term_id = $term[1]->term_taxonomy_id;
						}
						
						$term_array = get_term( $term_id, 'before-after-category' );
						$term_name = $term_array->name;
						$term_slug = $term_array->slug;
						
						remove_filter('wp_trim_excerpt', 'trim_excerpt');
						add_filter('wp_trim_excerpt', 'new_excerpt');
						$excerpt = get_the_excerpt();
						remove_filter('wp_trim_excerpt', 'new_excerpt');
						add_filter('wp_trim_excerpt', 'trim_excerpt');
						
						
						$features = get_post_meta(get_the_ID(), 'merged_image');
						
						if (!empty($features)) {?>
							<a href="<?php echo site_url() . '/before-after-category/' . $term_slug; ?>">	
							<?php foreach ($features as $feature) {
									$feature = explode("\n", $feature);?>
									<img class="cloudcarousel" title="<?php echo $excerpt; ?>" src=" <?php echo $feature[0] ?>" alt="Before and After <?php if ($term_name != '') {echo ' | ' . $term_name;} ?> <?php echo ' | ' . get_the_title(); ?>" />
								<?php } ?>
							</a>
					<?php } 
					}
				}
				
			}
			wp_reset_postdata();*/
			
		
			$args = array(
				'post_type' => 'page',
				'posts_per_page' => -1,
				/*'meta_key' => 'display_on_main', 
				'meta_value' => 'yes',
				'posts_per_page' => -1,
				*/
				'order' => 'ASC',
				'meta_key' => 'order_on_main_carousel',
				'orderby' => 'meta_value_num',
				'meta_query' => array(
					array(
						'key' => 'display_on_main',
						'value' => 'yes',
						'compare' => '='
					),
					array(
						'key' => 'order_on_main_carousel',
						'type' => 'numeric'
					)
				)	
			
			);
			
			$the_query = new WP_Query( $args );
			if( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) { 
					$the_query->the_post(); 
					
					
					$features = get_post_meta(get_the_ID(), 'main_carousel_description');
					//print_r('dasdsad');
					//echo $features[0];
					/*if (!empty($features)) {
						foreach ($features as $feature) {
								$feature = explode("\n", $feature);
							}
					}*/	
					
					
					
					/*remove_filter('excerpt_length', 'my_excerpt');
					add_filter('excerpt_length', 'tax_excerpt_length');
					remove_filter('wp_trim_excerpt', 'trim_excerpt');
					add_filter('wp_trim_excerpt', 'new_excerpt');*/
					?>
					<a href="<?php the_permalink(); ?>">
						<?php  the_post_thumbnail('full', array(
							'class' => 'cloudcarousel', 
							'alt' => get_the_title(), 
							//'title' => get_the_ID()
							'title' => $features[0]
						)); ?>
					</a>
					<?php 
					/*remove_filter('wp_trim_excerpt', 'new_excerpt');
					add_filter('wp_trim_excerpt', 'trim_excerpt');
					remove_filter('excerpt_length', 'tax_excerpt_length');
					add_filter('excerpt_length', 'my_excerpt');*/
					
				}
			}
		
		wp_reset_query();
		
		
		?>
	
	
		<?php
			if ( function_exists('dynamic_sidebar') && is_active_sidebar( 'carousel' ) ) {
				dynamic_sidebar('carousel') ;
			}
		?>
	</div>


	
</div>

