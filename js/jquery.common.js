jQuery(document).ready(function(){
	
	jQuery('.single-main').height(jQuery('#main').height() + 'px');	
	jQuery('.page-template-contact-page-php .left-sidebar').height(jQuery('.page-template-contact-page-php #main').height() + 4 + 'px');
	jQuery('#image-sidebar').height(jQuery('#main').height() + 'px');
	jQuery('.info-block').height(jQuery('#main').height() + 'px');
	jQuery('.page .info-block').height(jQuery('#main').height() + 'px');
	
	jQuery('.left-sidebar').css("height", jQuery('#main').height() + 'px');
	
	
	//jQuery('.carousel-bar').children().addClass("cloudcarousel");
	
	
	
	/** Placeholders **/
	jQuery('#your-name').val('Your Name(required)');
	jQuery('#your-name').live('focus', function() {
		if (this.value == 'Your Name(required)') {this.value = '';}
	});
	jQuery('#your-name').live('blur', function() {
		if (this.value == '') {this.value = 'Your Name(required)';}
	});

	jQuery('#your-email').val('Your Email(required)');
	jQuery('#your-email').live('focus', function() {
		if (this.value == 'Your Email(required)') {this.value = '';}
	});
	jQuery('#your-email').live('blur', function() {
		if (this.value == '') {this.value = 'Your Email(required)';}
	});
	
	jQuery('#daytime-phone').val('Daytime Phone(required)');
	jQuery('#daytime-phone').live('focus', function() {
		if (this.value == 'Daytime Phone(required)') {this.value = '';}
	});
	jQuery('#daytime-phone').live('blur', function() {
		if (this.value == '') {this.value = 'Daytime Phone(required)';}
	});
	
	jQuery('#cell-phone').val('Cell Phone(required)');
	jQuery('#cell-phone').live('focus', function() {
		if (this.value == 'Cell Phone(required)') {this.value = '';}
	});
	jQuery('#cell-phone').live('blur', function() {
		if (this.value == '') {this.value = 'Cell Phone(required)';}
	});
	
	jQuery('#procedure-interested').val('Procedure interested in');
	jQuery('#procedure-interested').live('focus', function() {
		if (this.value == 'Procedure interested in') {this.value = '';}
	});
	jQuery('#procedure-interested').live('blur', function() {
		if (this.value == '') {this.value = 'Procedure interested in';}
	});
	

	jQuery('#your-message').val('Your Message');
	jQuery('#your-message').live('focus', function() {
		if (this.value == 'Your Message') {this.value = '';}
	});
	jQuery('#your-message').live('blur', function() {
		if (this.value == '') {this.value = 'Your Message';}
	});
	
	
	
	jQuery('.slides-sidebar').jshowoff({
		controls: false,
		links: false,
		speed: 6000
	});
	
	
	
	
	jQuery(".carousel-bar").CloudCarousel({	
		xPos: 348,
		yPos: 60,
		xRadius: 150,
		yRadius: 25,
		buttonRight: jQuery("#left-but"),
		buttonLeft: jQuery("#right-but"),
		altBox: jQuery("#alt-text"),
		titleBox: jQuery("#title-text"),
		FPS: 50,
		autoRotate: 'no',
		autoRotateDelay: 15200,
		speed: 0.3,
		bringToFront: true,
		mouseWheel: true,
		minScale: 0.05,
		reflHeight: 86,
		reflGap: 10

	});


	
	
	jQuery(".main-carousel-bar").CloudCarousel({	
		xPos: 468,
		yPos: 60,
		xRadius: 300,
		yRadius: 25,
		buttonRight: jQuery("#left-but"),
		buttonLeft: jQuery("#right-but"),
		altBox: jQuery("#alt-text"),
		titleBox: jQuery("#title-text"),
		FPS: 50,
		autoRotate: 'no',
		autoRotateDelay: 12200,
		speed: 0.7,
		bringToFront: true,
		mouseWheel: true,
		minScale: 0.05,
		reflHeight: 86,
		reflGap: 10
	});	
	
	
	
	jQuery(".id1174").CloudCarousel({	
		xPos: 348,
		yPos: 60,
		xRadius: 250,
		yRadius: 25,
		buttonRight: jQuery("#left-but"),
		buttonLeft: jQuery("#right-but"),
		altBox: jQuery("#alt-text"),
		titleBox: jQuery("#title-text"),
		FPS: 50,
		autoRotate: 'no',
		autoRotateDelay: 12200,
		speed: 0.7,
		bringToFront: true,
		mouseWheel: true,
		minScale: 0.05,
		reflHeight: 86,
		reflGap: 10
	});
	
	
	
	
	
	/*jQuery(".term-carousel-bar").CloudCarousel({	
		xPos: 350,
		yPos: 60,
		xRadius: 137,
		yRadius: 25,
		buttonLeft: jQuery("#left-but"),
		buttonRight: jQuery("#right-but"),
		altBox: jQuery("#alt-text"),
		titleBox: jQuery("#title-text"),
		FPS: 50,
		autoRotate: 'left',
		autoRotateDelay: 12200,
		speed: 0.1,
		bringToFront: true,
		mouseWheel: true,
		minScale: 0.4,
		reflHeight: 86,
		reflGap: 10
	});
	

	jQuery(".term-carousel-one").CloudCarousel({	
		xPos: 350,
		yPos: 60,
		xRadius: 137,
		yRadius: 25,
		buttonLeft: jQuery("#left-but"),
		buttonRight: jQuery("#right-but"),
		altBox: jQuery("#alt-text"),
		titleBox: jQuery("#title-text"),
		FPS: 50,
		autoRotate: 'no',
		autoRotateDelay: 12200,
		speed: 0.1,
		bringToFront: true,
		mouseWheel: true,
		minScale: 0.4,
		reflHeight: 86,
		reflGap: 10
	});*/



		function fadeFromTo(from, to, fade_time) {
			jQuery(from).fadeOut(fade_time);
			jQuery(to).fadeIn(fade_time);
			return to;
		}
		
		jQuery(".left-video").children().hide();
		current = jQuery(".left-video").children().first();
		
		current.show();
		cur_link = jQuery(".right-video").children().first();
		cur = cur_link.children();
		jQuery(' .' + cur.attr("href")).show();
		cur.addClass("current");
		
		jQuery(".right-video li a").click(function (event) {
			event.preventDefault();
			/***/
			
			//console.log(cur);
			//console.log(cur.attr("href"));
			
			var prev_video = jQuery(' .' + cur.attr("href"));
			//prev_video.children().get(0).stopVideo();
			
			var last_video = jQuery(".left-video").children().last();
			
			
			
			
			//console.log(prev_video);
			
			
			/****/
			
			//jQuery(' .' + cur.attr("href")).remove();
			
			/***/
			jQuery(cur.attr("href")).remove();
			
			jQuery(".right-video li a").removeClass("current");
			
			var next = ' .' + jQuery(this).attr("href");
			
			jQuery(this).addClass("current");
			current = fadeFromTo(current, next, 200);
			
			
			
			
		});


});
