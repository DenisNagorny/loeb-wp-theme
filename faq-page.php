<?php
/*
 * Template Name: FAQ Template
 */

get_header(); ?>
	
	<div id="main" class="<?php if(!is_subpage()) echo ' is_not_subpage '; ?> <?php if (!has_children($post->ID)) echo ' not_has_children'; ?>">
		
		
		<div class="info-block alignleft">
		
			<?php if ((is_page_template('contact-page.php') || is_page()) && !is_front_page()) { ?>
				<div class="bread-crumbs">
						<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
				</div>
			<?php } ?>
			
			
			<div class="left-block">
				<?php 
					if ((has_children($post->ID) || $post->post_parent != '')) {
						get_sidebar('left'); 
					}
				?>
				
				
				<div class="single-main alignleft ">
					<?php 
						if (have_posts()) {
							while (have_posts()) {
								the_post();?>
								<div class="post">
									<?php if (has_post_thumbnail()) { ?><div class="featured-image"> <?php the_post_thumbnail('post-thumbnail'); ?> </div><?php }?>
									<h2 class="title"><?php the_title();?> </h2>
									<div class="content"> <?php the_content(); ?> </div>
								</div>
							<?php }
						}
					?>
				</div>
			</div>
		
		</div>
		
		
		
		<?php // get_sidebar('right'); ?>
	</div>
<?php get_footer(); ?>