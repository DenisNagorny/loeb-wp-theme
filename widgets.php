<?php
/* Show recent image widget */


class Bottom_Menu_Widget extends WP_Widget {
	/**
	* Widget setup.
	*/
	function Bottom_Menu_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'bottom_menu', 'description' => __('A widget that displays a bottom menu.', 'bottom_menu') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 430, 'height' => 350, 'id_base' => 'bottom_menu' );

		/* Create the widget. */
		$this->WP_Widget( 'bottom_menu', __('Bottom menu', 'bottom_menu'), $widget_ops, $control_ops );
	}

	/**
	* How to display the widget on the screen.
	*/
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title ) {
			echo $before_title;

			echo $title;

			echo $after_title;
		}?>
		<div class="text">
			
			<?php
				$args = array(
					'menu' => 'Bottom Menu',
					'echo' => 0,
					'container' => '',
				);
				$separator = '<span class="separator">|</span>';
				$pattern = '/(<\\/a>).*?(<\\/li>).*?(<li)/is';
				$replace = '</a>' . $separator . '</li><li';
				$menu = preg_replace( $pattern, $replace, wp_nav_menu($args) );
				echo $menu;
			?>
			
		</div>		
		<?php 
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title, icon url and icon alt to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] ); 
		$instance['selected_page'] = strip_tags( $new_instance['selected_page'] );       	
		return $instance;
	}

	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array(
			'title' => 'Bottom menu'
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
	<?php

	}
} add_action('widgets_init', create_function('', 'return register_widget("Bottom_Menu_Widget");'));









class Subpages_Widget extends WP_Widget {

	/**
	* Widget setup.
	*/
	
	function Subpages_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'subpages-widget', 'description' => __('A widget that displays a subpages of parent page', 'subpages') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 430, 'height' => 350, 'id_base' => 'subpages-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'subpages-widget', __('Subpages', 'subpages'), $widget_ops, $control_ops );
	}

	/**
	* How to display the widget on the screen.
	*/
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
		echo $before_widget;
		?>

		
		<?php 
		$thumb = get_the_post_thumbnail($instance['page_id'], 'post-taxonomy' );
		$link = get_permalink($instance['page_id']);
		if ( $thumb ) {?>
			<a href="<?php echo $link; ?> "><?php echo $thumb; ?></a>
		<?php 
		}
		
		if ( $title ) {
			echo $before_title; 

			echo '<a href="'.$link . '">' . $title . '</a>';

			echo $after_title;
		}

		

		
		$args = array(
			'post_parent' => $instance['page_id'],
			'posts_per_page' => $instance['numberposts'],
			'post_type' => 'page',
			'post_status' => 'publish',
			'order' => 'ASC',
			'orderby' => 'menu_order'
		);
		echo '<ul>';
		$children = new WP_Query($args);
		if ( $children->have_posts() ) {
			while ( $children->have_posts() ) { 
				$children->the_post(); 
				?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
		<?php
			}
		}
		echo '</ul>';
		
		
		echo $after_widget;
	}
	

	/**
     * Update the widget settings.
     */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title, icon url and icon alt to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
			if (!is_numeric($new_instance['numberposts'])) 
		$instance['numberposts'] = -1;
			else  
			{$instance['numberposts'] = strip_tags( $new_instance['numberposts'] );};
		
		$instance['page_id'] = strip_tags( $new_instance['page_id'] );
		return $instance;
	}

	/**
	* Displays the widget settings controls on the widget panel.
	* Make use of the get_field_id() and get_field_name() function
	* when creating your form elements. This handles the confusing stuff.
	*/
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array(
			'title' => 'Subpages',
			'post_type'=> 'pages',
			'numberposts' => -1,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );  ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Count posts -->

		<p>
			<label for="<?php echo $this->get_field_id( 'numberposts' ); ?>"><?php _e('Number of posts to show:', 'recent_cat_posts'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'numberposts' ); ?>" name="<?php echo $this->get_field_name( 'numberposts' ); ?>" 
			value="<?php echo $instance['numberposts']; ?>" />
		</p>
		
		<!-- Get Term Id -->
		
		<p>
			<label for="<?php echo $this->get_field_id( 'page_id' ); ?>"><?php _e('Pages:', 'page_id'); ?></label>
			<?php 
			wp_dropdown_pages(array(
				'hierarchical' => 1,
				'depth' => 1,
				'name' => $this->get_field_name( 'page_id' ),
				'id' => $this->get_field_id( 'page_id' ),
				'selected' => $instance['page_id']
			)); 
			?>
		</p>

	<?php

	}
} add_action('widgets_init', create_function('', 'return register_widget("Subpages_Widget");'));





