<?php get_header(); ?>
	
	<div id="main">
			<div class="main alignleft">
				
				<?php 
					if (have_posts()) {
							while (have_posts()) {
								the_post();
								?>
								<div class="post clearfloat <?php if (has_post_thumbnail())  { echo 'has_thumb'; } ?>">
									<?php if (has_post_thumbnail()) { ?>
										<div class="featured-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'featured-image'); ?></a></div>
									<?php }?>
									<h2 class="title"><?php the_title();?></h2>
									<?php /*
									<div class="meta">
										<?php the_author(); ?> | <?php the_time("m/d/y")/* ?> | Posted In <?php the_category(', ');  ?>
									</div>
									*/ ?>
									<div class="content"><?php the_content(); ?></div>
								</div> 
						<?php   }
					}
				?>
				
			</div>
		<?php get_sidebar('right'); ?>
	</div>
	
<?php get_footer(); ?>