<?php
include( TEMPLATEPATH.'/widgets.php' );
/*
 * Adding Sidebars
 */
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Right Sidebar',
		'id' => 'right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Carousel Bar',
		'id' => 'carousel',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Left Sidebar',
		'id' => 'left',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Footer Sidebar',
		'id' => 'infooter',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Home Images Sidebar',
		'id' => 'home-images',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Slides Sidebar',
		'id' => 'slides',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Blog Sidebar',
		'id' => 'blog',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
	register_sidebar(array(
		'name' => 'Modifications Sidebar',
		'id' => 'modifications',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'before_title'  => '<div class="title">',
		'after_title'   => '</div>',
		'after_widget'  => '</div>'
	));
}


function register_menus() {
	register_nav_menu('top_menu', __('Top Menu'));
	register_nav_menu('bottom_menu', __('Bottom Menu'));
	register_nav_menu('home_menu', __('Home Menu'));
	register_nav_menu('home_left_menu', __('Home Left Menu'));
} add_action('init', 'register_menus');

add_theme_support( 'post-thumbnails' );
add_image_size ('featured-image', 673, 255, true);
//add_image_size ('post-thumbnail', 173, 173, true);
add_image_size ('post-taxonomy', 163, 172, true);
add_image_size ('post-taxonomy', 163, 172, true);
add_image_size ('single-image', 150, 150, true);

add_image_size ('thumb', 100, 100, true);


add_image_size('after-image-thumbnail', 173, 173, false);
add_image_size('before-image-thumbnail', 173, 173, false);


set_post_thumbnail_size(173, 173, true);
/*
 * Post excerpt 'read more' link
 */
function my_excerpt_length($length) {
	return 45;
}
function tax_excerpt_length($length) {
	return 25;
}
add_filter('excerpt_length', 'my_excerpt_length');

function trim_excerpt($excerpt) {
	$excerpt = str_replace('[...]', '', $excerpt);
	$excerpt = str_replace('...', '', $excerpt);
	return $excerpt .= '... <nobr><a href="'. get_permalink($post->ID) . '" title="Continue to ' . $post->post_title . '" class="read-more-link">Read more &raquo;</a></nobr>';
}

function new_excerpt($excerpt) {
	$excerpt = str_replace('[...]', '', $excerpt);
	$excerpt = str_replace('...', '', $excerpt);
	return $excerpt;
}
add_filter('wp_trim_excerpt', 'trim_excerpt');

function update_widget_css_class( $parameters ) {
	global $wp_registered_sidebars, $wp_registered_widgets;

	$sidebars_widgets = wp_get_sidebars_widgets();
	$sidebar_id = $parameters[0]['id'];

	// Checkinging the position of current widget in the sidebar (it's index starting with 1)
	$widget_index = null;
	$num_widgets_in_sidebar = count($sidebars_widgets[$sidebar_id]);
	foreach ( $sidebars_widgets[$sidebar_id] as $index=>$widget_id ) {
		if ( $widget_id == $parameters[0]['widget_id'] ) {
			$widget_index = $index + 1;
				break;
		}
	}

	// Creating additional CSS class for widget with it's position index in the sidebar
	if ( $widget_index ) {
		$position_class = "widget-$widget_index ";
		if ( $widget_index == 1 ) $position_class .= 'first ';
		if ( $widget_index == $num_widgets_in_sidebar ) $position_class .= 'last ';

	// Updating widget class
		$pattern = '/(.* class=[\'"])(.*)/i';
		$replace = '$1' . $position_class . '$2';
		$parameters[0]['before_widget'] = preg_replace($pattern, $replace, $parameters[0]['before_widget']);
	}

	return $parameters;
}
add_filter('dynamic_sidebar_params', 'update_widget_css_class');

/************************** First - Last Classes For Menus ***********************/

add_filter( 'wp_nav_menu_items', 'first_last_class' );

function first_last_class( $items ) {
	$first = strpos( $items, 'class=' );
	if( false !== $first )
		$items = substr_replace( $items, 'first ', $first+7, 0 );
	$last = strripos( $items, 'class=');
	if( false !== $last )
		$items = substr_replace( $items, 'last ', $last+7, 0 );
	return $items;
}

/** Check page on subpages **/

function has_children($post_id) {
	$children = get_pages("child_of=$post_id");
	if( count( $children ) != 0 ) { return true; } 
	else { return false; }
}

function is_subpage() {
	global $post;
	if ( is_page() && $post->post_parent ) {
		return true;
	} else {
		return false;
	}
}

/** Add JS **/

function theme_scripts() {
	wp_register_script( 'jshowoff', get_bloginfo('template_directory') . '/js/jquery.jshowoff.min.js', array('jquery'), '0.1.2', false );
	
	wp_register_script( 'slimbox', get_bloginfo('template_directory') . '/js/slimbox2.js', array('jquery'), '2.0.3', false );
	wp_register_script( 'mousewheel', get_bloginfo('template_directory') . '/js/jquery.mousewheel.js', array('jquery'), '3.0.2', false );
	
	wp_register_script( 'carousel', get_bloginfo('template_directory') . '/js/cloud5.js', array('jquery'), '1.0.5', false );
	wp_register_script( 'jquery_common', get_bloginfo('template_directory') . '/js/jquery.common.js', array('jquery', 'jshowoff', 'carousel'), '1.0', false );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jshowoff' );
	
	wp_enqueue_script( 'slimbox' );
	wp_enqueue_script( 'mousewheel' );
	
	wp_enqueue_script( 'carousel' );
	wp_enqueue_script( 'jquery_common' );
}

add_action('init', 'theme_scripts');


/** Add Before and After CPT **/
/*
add_action( 'init', 'create_post_type' );

function create_post_type() {
	 $labels = array(  
	'name' => 'Modification',
	'singular_name' => 'Modification',
	'add_new' => 'Add modification',  
	'add_new_item' => 'Add new modification',
	'edit_item' => 'Edit modification',
	'new_item' => 'New modification',
	'view_item' => 'Show modification',
	'search_items' => 'Find modification',
	'not_found' => 'Modification not found',
	'not_found_in_trash' => 'Modification not found',
	'parent_item_colon' => '',  
	'menu_name' => 'Modifications'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array('title','editor','author','thumbnail','excerpt','comments', 'custom-fields'),
		'rewrite' => array( 'slug' => 'before-after' ),
	);
	register_post_type('modification',$args);
}

/** Register Before and After Taxonomy **/
/*
function register_before_and_after_taxonomy() {
	$labels = array(
		'name' => 'Modification Categories', 'taxonomy general name',
		'singular_name' => 'Modification Category', 'taxonomy singular name',
		'search_items' =>  'Search Modification Categories',
		'all_items' => 'All Modification Categories',
		'parent_item' => 'Parent Modification Category',
		'parent_item_colon' => 'Parent Modification Category:',
		'edit_item' =>  'Edit Modification' , 
		'update_item' =>  'Update Modification Category' ,
		'add_new_item' =>  'Add New Modification Category' ,
		'new_item_name' =>  'New Modification Category Name' ,
		'menu_name' =>  'Modification Category' ,
	);
	register_taxonomy('before-after-category', array('modification'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'before-after-category' ),
	));	
} add_action('init', 'register_before_and_after_taxonomy');
*/

/** Multiple CPT **/
/*
if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(
		array(
			'label' => 'Before Featured Image',
			'id' => 'before-image',
			'post_type' => 'modification'
		)
	);
	new MultiPostThumbnails(
		array(
			'label' => 'After Featured Image',
			'id' => 'after-image',
			'post_type' => 'modification'
		)
	);
}
*/

/*class mainMenuWalker extends Walker_Nav_Menu { 
	function start_el(&$output, $item, $depth, $args) { 
		$class_names = join( ' ', $item->classes ); 
		$class_names = ' class="' .esc_attr( $class_names ). '"'; 
		$output.= '<li id="menu-item-' . $item->ID . '"' .$class_names. '>'; 

		$attributes.= !empty( $item->url ) ? ' href="' .esc_attr($item->url). '"' : ''; 
		$item_output = $args->before; 

		$current_url = (is_ssl()?'https://':'http://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 
		$item_url = esc_attr( $item->url ); 
		if ( $item_url != $current_url ) $item_output.= '<a'. $attributes .'>'.$item->title.'</a>'; 
		else $item_output.= $item->title; 

		$item_output.= $args->after; 
		$output.= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args ); 
	}
}*/


/** Merge two images **/

function merge_two_images($before, $after) {
	switch (exif_imagetype($before[0])) {
		case  IMAGETYPE_GIF: $image_before = imagecreatefromgif($before[0]); break;
		case IMAGETYPE_JPEG: $image_before = imagecreatefromjpeg($before[0]); break;
		case  IMAGETYPE_PNG: $image_before = imagecreatefrompng($before[0]); break;
	}
	
	switch (exif_imagetype($after[0])) {
		case  IMAGETYPE_GIF: $image_after = imagecreatefromgif($after[0]); break;
		case IMAGETYPE_JPEG: $image_after = imagecreatefromjpeg($after[0]); break;
		case  IMAGETYPE_PNG: $image_after = imagecreatefrompng($after[0]); break;
	}
	
	
	$max_height = ($after[2] > $before[2]) ? $after[2] : $before[2];
	
	$dest = imagecreatetruecolor($before[1] + $after[1] + 1, $max_height);
	
	imagecopy($dest, $image_before, 0, 0, 0, 0, $before[1], $before[2]);
	imagecopy($dest, $image_after, $before[1] + 1, 0, 0, 0, $after[1], $after[2]);
	$path = wp_upload_dir();
	
	$merged_name = $path['path'] .'/'. basename($before[0]) . '-' . basename($after[0]) . '.jpg';
	$url_image = $path['url'] . '/' . basename($before[0]) . '-' . basename($after[0]) . '.jpg';
	
	//$merged_name = $path['path'] .'/image-'. get_the_ID() . '.jpg';
	//$url_image = $path['url'] . '/image-' . get_the_ID() . '.jpg';
	
	
	$dest = imagejpeg($dest, $merged_name, 100);
	
	return $url_image;
}

/** Add to Post Third CPT (Merged image - Before and After)**/


function create_merged_image($post_ID, $post) {
	
	$before_image_id = get_post_meta( $post_ID, 'modification_before-image_thumbnail_id', true);
	$after_image_id = get_post_meta( $post_ID, 'modification_after-image_thumbnail_id', true);
	
	$before_image = wp_get_attachment_image_src($before_image_id);
	$after_image = wp_get_attachment_image_src( $after_image_id);
	
	//print_r('Before imagess: ' . $before_image . 'After image: ' . $after_image);
	//die();
	
	if (($before_image != '') && ($after_image != '')) { 
		$merged_image = merge_two_images($before_image, $after_image);
	
		if (!get_post_meta(get_the_ID(), "merged_image", true)) {
			/*add_post_meta($post->ID, 'merged_image', $merged_image) or*/ update_post_meta($post->ID, 'merged_image', $merged_image); 
		}
	}
}
add_action('save_post', 'create_merged_image', 10, 2);

/*****/


function is_parent_category() {
	global $wpdb;	
	$term = get_queried_object();
	if ($term->parent == 0) {
		return true;	
	} else { 
		return false;
	}
}



/****/

add_post_type_support( 'page', 'excerpt' );



/**************************************************************************/

function add_fist_last_item_class( $myitems ) {
	reset($myitems);
	$first_key = key($myitems);
	$last_key = count($myitems);
	$myitems[$first_key]->classes[] = 'first ';
	$myitems[$last_key]->classes[] = 'last';
	return $myitems;
}
add_filter( 'wp_nav_menu_objects', 'add_fist_last_item_class' );



/*
 Policy custom post type
 */
function policy_post_type() {
	register_post_type('policy',
			array('labels' => array(
					'name' => __('Policies', 'madeoftubes'), /* This is the Title of the Group */
					'singular_name' => __('Policy', 'madeoftubes'), /* This is the individual type */
					'all_items' => __('All Policies', 'madeoftubes'), /* the all items menu item */
					'add_new' => __('Add New', 'madeoftubes'), /* The add new menu item */
					'add_new_item' => __('Add New Policy', 'madeoftubes'), /* Add New Display Title */
					'edit' => __( 'Edit', 'madeoftubes' ), /* Edit Dialog */
					'edit_item' => __('Edit Policies', 'madeoftubes'), /* Edit Display Title */
					'new_item' => __('New Policy', 'madeoftubes'), /* New Display Title */
					'view_item' => __('View Policy', 'madeoftubes'), /* View Display Title */
					'search_items' => __('Search Policies', 'madeoftubes'), /* Search Custom Type Title */
					'not_found' =>  __('No policies found.', 'madeoftubes'), /* This displays if there are no entries yet */
					'not_found_in_trash' => __('Nothing found in Trash', 'madeoftubes'), /* This displays if there is nothing in the trash */
					'menu_name' => __('Policies', 'madeoftubes'),
					'parent_item_colon' => ''
			), /* end of arrays */
					'description' => __( 'List of platform policies', 'madeoftubes' ), /* Custom Type Description */
					'public' => true,
					'publicly_queryable' => true,
					'exclude_from_search' => false,
					'show_ui' => true,
					'query_var' => true,
					'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
					'menu_icon' => get_bloginfo('template_url') . '/images/custom-post-icon.png', /* the icon for the custom post type menu */
					'rewrite'	=> array( 'slug' => 'policies', 'with_front' => false ), /* you can specify it's url slug */
					'has_archive' => 'policies', /* you can rename the slug here */
					'capability_type' => 'post',
					'show_in_nav_menus' => true,
					'hierarchical' => false,
					/* the next one is important, it tells what's enabled in the post editor */
					'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
					//'supports' => 'revisions',
					
	) /* end of options */
	);

}
add_action('init', 'policy_post_type');
